<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Articles extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->Model('Article');
        if(!isset($_SESSION['log'])){
            redirect('back-office');
        }
    }

    public function index(){
        $data['content'] = 'article-liste';
        $data['articles'] = $this->Article->findAll();
        $this->load->view('back-office/template', $data);
    }
    
    public function creation(){
        $data['content'] = 'article-creation';
        $this->load->view('back-office/template', $data);
    }

    public function maj($id){
        $data['content'] = 'article-creation';
        $data['article'] = $this->Article->findById($id);
        $this->load->view('back-office/template', $data);
    }

    public function save(){
        $titre = $this->input->post('titre');
        $contenue = $this->input->post('contenue');
        $motsCle = $this->input->post('motsCle');
        $date = $this->input->post('date');
        $newArticle = new Article(0, $titre, $contenue, $date, $motsCle);
        $newArticle->save();
        redirect('back-office');
    }

    public function update(){
        $idArticle = $this->input->post('idArticle');
        $titre = $this->input->post('titre');
        $contenue = $this->input->post('contenue');
        $motsCle = $this->input->post('motsCle');
        $date = $this->input->post('date');
        $newArticle = new Article($idArticle, $titre, $contenue, $date, $motsCle);
        $newArticle->update();
        redirect('back-office');
    }
}
