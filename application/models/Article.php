<?php
    if (!defined('BASEPATH')) exit('No direct script access allowed');

    class Article extends CI_Model{
        // Construstor
        public function __construct($idArticle = '', $titre = '', $contenue = '', $date = '', $motsCle = ''){
            parent:: __construct();
            $this->setIdArticle($idArticle);
            $this->setTitre($titre);
            $this->setContenue($contenue);
            $this->setDate($date);
            $this->setMotsCle($motsCle);
        }

        private $_idArticle;
        private $_titre;
        private $_contenue;
        private $_date;
        private $_motsCle;

        public function setIdArticle($idArticle){
            $this->_idArticle = $idArticle;
        }

        public function setTitre($titre){
            $this->_titre = $titre;
        }

        public function setContenue($contenue){
            $this->_contenue = $contenue;
        }

        public function setDate($date){
            $this->_date = strtotime($date);
        }

        public function setMotsCle($motsCle){
            $this->_motsCle = $motsCle;
        }

        public function getIdArticle(){
            return $this->_idArticle;
        }

        public function getTitre(){
            return $this->_titre;
        }

        public function getContenue(){
            return $this->_contenue; 
        }

        public function getContenueFormate(){
            $contenue = $this->_contenue; 
            $motsCle = explode(",", $this->getMotsCle());
            foreach ($motsCle as $mot) 
                $contenue = str_replace(trim($mot), "<strong>" .trim($mot). "</strong>" , $contenue );
                return $contenue;
        }

        public function getSubContenue($nombre){
            $contenue = $this->getContenue();
            return substr($contenue, 0, $nombre) . ((strlen($contenue) > $nombre) ? "..." : "");
        }

        public function getDate(){
            return $this->_date;
        }

        public function getMotsCle(){
            return $this->_motsCle;
        }

        public function findAll(){
            $articles = array();

            $sql = "SELECT * FROM ARTICLE ORDER BY DATEARTICLE DESC";
            $query = $this->db->query($sql);
            if($this->db->error()['code'] != 0)
                throw new Exception($this->db->error()['message']);
            else{
                foreach ($query->result_array() as $row)
                array_push($articles, new Article($row['IDARTICLE'], $row['TITRE'], $row['CONTENUE'], $row['DATEARTICLE'], $row['MOTSCLE']));
            }
            
            return $articles;
        }

        public function findById($id){
            $article;

            $sql = "SELECT * FROM ARTICLE WHERE IDARTICLE = %s";
            $sql = sprintf($sql, $id);
            $query = $this->db->query($sql);
            if($this->db->error()['code'] != 0)
                throw new Exception($this->db->error()['message']);
            else{
                foreach ($query->result_array() as $row)
                    $article = new Article($row['IDARTICLE'], $row['TITRE'], $row['CONTENUE'], $row['DATEARTICLE'], $row['MOTSCLE']);
            }
            return $article;
        }

        public function findLastNews(){
            $articles = array();

            $sql = "SELECT * FROM ARTICLE ORDER BY DATEARTICLE DESC LIMIT 5";
            $query = $this->db->query($sql);
            if($this->db->error()['code'] != 0)
                throw new Exception($this->db->error()['message']);
            else{
                foreach ($query->result_array() as $row)
                array_push($articles, new Article($row['IDARTICLE'], $row['TITRE'], $row['CONTENUE'], $row['DATEARTICLE'], $row['MOTSCLE']));
            }
            
            return $articles;
        }

        public function save(){
            $sql="INSERT INTO ARTICLE (TITRE, CONTENUE, DATEARTICLE, MOTSCLE) VALUES ('%s','%s','%s','%s')";
            $sql=sprintf($sql, $this->getTitre(), $this->_contenue, date("Y-m-d", $this->getDate()), $this->getMotsCle());
            $query = $this->db->query($sql);
            if($this->db->error()['code'] != 0)
                throw new Exception($this->db->error()['message']);
        }

        public function update(){
            $sql="UPDATE ARTICLE SET TITRE = '%s', CONTENUE = '%s', DATEARTICLE = '%s', MOTSCLE = '%s' WHERE IDARTICLE = %s";
            $sql=sprintf($sql, $this->getTitre(), $this->_contenue, date("Y-m-d", $this->getDate()), $this->getMotsCle(), $this->getIdArticle());
            $query = $this->db->query($sql);
            if($this->db->error()['code'] != 0)
                throw new Exception($this->db->error()['message']);
        }
    }
    
?>