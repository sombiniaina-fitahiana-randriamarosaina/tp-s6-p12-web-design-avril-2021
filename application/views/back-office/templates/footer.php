                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Your Website 2021</div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="<?php echo site_url(); ?>/assets/Bootstrap/Bootstrap/jquery-3.5.1.slim.min.js" crossorigin="anonymous"></script>
        <script src="<?php echo site_url(); ?>/assets/Bootstrap/Bootstrap/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="<?php echo site_url(); ?>/assets/js/scripts.js"></script>
    </body>
</html>